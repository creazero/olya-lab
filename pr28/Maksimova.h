﻿#pragma once
#include <string>

using namespace std;

class Maksimova
{
protected:
	string name;
	float experience;
	int hourly_wage,
	    hours_worked;
public:
	// почистил класс, убрав всякие бесполезные пустые конструкторы и деструкторы

	// конструктор
	Maksimova(string _name, float experience, int _hourly_wage, int _hours_worked);

	// переделываем рабочего (бедный он)
	virtual void setWorker(string _name, float experience, int _hourly_wage, int _hours_worked);

	// получаем инфу о рабочем
	virtual void getWorker();
};

