﻿#pragma once
#include "Maksimova.h"

class Maksimova_1 :public Maksimova
{
private:
	int salary;
	float premy;
public:
	Maksimova_1(string _name, float experience, int _hourly_wage, int _hours_worked);
	void setWorker(string _name, float experience, int _hourly_wage, int _hours_worked);
	void getWorker();

	// рассчитываем зп и премию рабочего
	void calculate();
};

