﻿#include "stdafx.h"
#include "Maksimova_1.h"
#include <iostream>

// ВЫЗЫВАЕМ КОНСТРУКТОР РОДИТЕЛЯ, ЧТОБЫ НЕ ПОВТОРЯТЬ КОД
// так у нас name, experience и тд будут содержать правильные значения
Maksimova_1::Maksimova_1(string _name, float experience, int _hourly_wage, int _hours_worked)
	: Maksimova(_name, experience, _hourly_wage, _hours_worked)
{
	calculate();
}

void Maksimova_1::setWorker(string _name, float _experience, int _hourly_wage, int _hours_worked)
{
	name = _name;
	experience = _experience;
	hourly_wage = _hourly_wage;
	hours_worked = _hours_worked;
	calculate();
}

void Maksimova_1::getWorker()
{
	cout << "Salary is " << salary << endl;
	cout << "Premy is " << premy << endl;
}

void Maksimova_1::calculate()
{
	salary = hourly_wage * hours_worked;
	float p;
	if (experience < 1) p = 0;
	else
	{
		if (experience >= 1 && experience < 3) p = 0.05*salary;
		else
		{
			if (experience >= 3 && experience < 5) p = 0.08*salary;
			else
			{
				if (experience > 5) p = 0.15*salary;
			}
		}
	}
	premy = p;
}
	
		
	