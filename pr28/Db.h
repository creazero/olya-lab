#pragma once
#include "Maksimova.h"
#include <list>

class Db
{
private:
	list <Maksimova*> l;
public:
	Db();
	bool add(Maksimova* m);
	list<Maksimova*>::iterator erase(list<Maksimova*>::iterator);
	list <Maksimova*>::iterator begin();
	list <Maksimova*>::iterator end();
	~Db();
};

