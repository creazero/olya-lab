// pr28.cpp : Defines the entry point for the console application.
//


#include "Maksimova.h"
#include "Maksimova_1.h"
#include "db.h"

#include <iostream>
#include <string>
#include <list>

#include "Db.h"

using namespace std;

void issledovanie(Db&);

int main()
{
	string name = "Olga";
	float experience = 4.6;
	int hourly_wage = 26,
		hours_worked = 50;
	Maksimova* m = new Maksimova(name, experience, hourly_wage, hours_worked);
	Maksimova_1* n = new Maksimova_1("Very Olga", 5.4, 50, 39);
	Db db;
	db.add(n);
	db.add(m);

	issledovanie(db);

	return 0;
}

void issledovanie(Db& db) 
{
	for (list<Maksimova*>::iterator it = db.begin(); it != db.end(); it++) 
	{
		(*it)->getWorker();
		cout << typeid(*it).name() << endl;
		cout << endl;
	}
}



