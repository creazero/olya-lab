#include "stdafx.h"
#include "Maksimova.h"
#include <string>
#include <iostream>

Maksimova::Maksimova(string _name, float _experience, int _hourly_wage, int _hours_worked)
{
	name = _name;
	experience = _experience;
	hourly_wage = _hourly_wage;
	hours_worked = _hours_worked;
}


void Maksimova::setWorker(string _name, float _experience, int _hourly_wage, int _hours_worked)
{
	name = _name;
	experience = _experience;
	hourly_wage = _hourly_wage;
	hours_worked = _hours_worked;
}

void Maksimova::getWorker() 
{
	cout << name << endl;
	cout << "Experience is " << experience << endl;
	cout << "Hourly wage is " << hourly_wage << endl;
	cout << name << " has worked " << hours_worked << " hours" << endl;
}
